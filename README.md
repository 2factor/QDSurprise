## Quality Diversity Through Surprise 

Source code for Quality Diversity Through Surprise.

##  Algorithm:

![Alt-Text](/uploads/74e02ac4b9993be76d92f758810d79c4/fig_1_color.png)

## Publication:

D. Gravina, A. Liapis and G. N. Yannakakis, "Quality Diversity Through Surprise," in IEEE Transactions on Evolutionary Computation, 2018

DOI: 10.1109/TEVC.2018.2877215

## Requirements

C++11 compiler.

## IDE

Source code has been written and tested with CLion 2017.2.3

## Domain

Mazes used in the experiments are available at https://gitlab.com/2factor/QDMazes

## Contact:

Daniele Gravina (daniele.gravina@um.edu.mt)

## Additional Notes

The provided source code is based on the original novelty search source code provided by Joel Lehman and Kenneth O. Stanley [1].

## References

[1] Lehman, Joel, and Kenneth O. Stanley. "Abandoning objectives: Evolution through the search for novelty alone." Evolutionary computation 19.2 (2011): 189-223.

----
