#ifndef MAZE_H
#define MAZE_H
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
using namespace std;

//simple point class
class Point
{
    public:
        Point(float x1,float y1);
        Point();	
        Point(const Point& k);	
        void fromfile(ifstream& file);
        //determine angle of vector defined by (0,0)->This Point
        float angle();
        //rotate this point around another point
        void rotate(float angle,Point p);
        //distance between this point and another point
        float distance(Point b);
    
    float x;
    float y;
};

//simple line segment class, used for maze walls
class Line
{
    public:
    Line(Point k,Point j);
    Line(float x1,float y1,float x2,float y2);
    Line(Point k,float x2,float y2);
    Line(float x1,float y1,Point j);
    Line(ifstream& file);
    Line();
    //midpoint of the line segment
    Point midpoint();
 
    //return point of intersection between two line segments if it exists 
    Point intersection(Line L,bool &found);
    
    //distance between line segment and point
    float distance(Point n);
    //line segment length
    float length();
    
    Point a;
    Point b;
};

//class for the maze navigator
class Character
{
    public:
        vector<float> rangeFinderAngles; //angles of range finder sensors
        vector<float> radarAngles1; //beginning angles for radar sensors
        vector<float> radarAngles2; //ending angles for radar sensors

        vector<float> radar; //stores radar outputs
        vector<float> rangeFinders; //stores rangefinder outputs
        Point location;
        float heading;
        float speed;
        float ang_vel;
        float radius;
        float rangefinder_range;
        
        Character();
};

//all-encompassing environment class
class Environment
{
    public:
        Environment(const Environment &e);
	//initialize environment from maze file
        Environment(const char* filename,bool useBorders=false);
	//debug function
        void display();
        //used for fitness calculations
        float distance_to_target();
	//create neural net inputs from sensors
	void generate_neural_inputs(double* inputs);
	//transform neural net outputs into angular velocity and speed
	void interpret_outputs(float o1,float o2);
	//run a time step of the simulation
        void Update();
        //see if navigator is outside the borders
        bool outOfBounds(float loc_x, float loc_y);
        inline bool outOfBounds(Point loc){ return outOfBounds(loc.x,loc.y); }
        inline bool outOfBounds(vector<float> loc){ return outOfBounds(loc.at(0),loc.at(1)); }
        //see if navigator is outside the borders
        float distFromBounds(float loc_x, float loc_y);
        inline float distFromBounds(Point loc){ return distFromBounds(loc.x,loc.y); }
        inline float distFromBounds(vector<float> loc){ return distFromBounds(loc.at(0),loc.at(1)); }
	//see if navigator has hit anything
        bool collide_lines(Point loc,float rad);
	//rangefinder sensors
        void update_rangefinders(Character& h);
        //radar sensors
        void update_radar(Character& h);
        
        ~Environment();
		
        vector<Line*> lines; //maze line segments
        Character hero; //navigator
        Point end,start; //the goal
	int reachgoal;
        Line* borders; // top left, bottom right
};
#endif
