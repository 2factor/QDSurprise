#include <iostream>
#include <vector>
#include <cstring>

#include <unistd.h>	
#include "neat.h"
#include "helpers.h"
#include "population.h"
#include "noveltyexp.h"
#include "noveltyset.h"

#include "surprise_math.h"

using namespace std;

void makeDirs(char* s) {
	const string dirname = string(s);
    vector<string> elems;
    split(s, '/', elems);
	stringstream inc_filefolder;
	inc_filefolder << "";
	for(vector<string>::iterator it = elems.begin(); it != elems.end(); ++it) {
		if((*it).empty()){ continue; }
		inc_filefolder << *it << "//";
		char command[250] = {};
		// then remake them
		stringstream ss;
		ss << "mkdir \"" << inc_filefolder.str() << "\"";
		string temp_s = ss.str();
		const char* temp_c = temp_s.c_str();
		system(temp_c);
		
	}
}

void runExperiment(int maze_choice, int optimization_choice, int runs, float ageDecay=1.f, int maxPast=-1, int nearestNeighbors=-1){
    char mazename[100]="hard_maze.txt";
    char filename[100]=".//logs//";
    char settingsname[100]="maze.ne";

    int param=(-1);		// no params for this experiment
    bool useBorders = false;

    //srand( (unsigned)time( NULL )  + getpid());
    //srand(1);

    NEAT::Population *p;
    NEAT::load_neat_params(settingsname,true);

    int MAZE_TYPE = 0;


    std::strcat(filename,"deceptive_resolution_4");

	if (MAZE_TYPE == 0)
	{
		std::strcat(filename, "//");
	}
	else if (MAZE_TYPE == 1){
		std::strcat(filename, "_easy//");
	}

    if(optimization_choice==1){

        int index_maze = 9;

        for(int run = index_maze;run<=index_maze;run++){

            for(int j=1;j<=50;j++){

                if (MAZE_TYPE == 0){
                    strcpy(mazename,"QDMazes/Mazes/maze");
                }
                else{
                    strcpy(mazename,"QDMazes/Sensitivity/maze");
                }
                strcat(mazename, std::to_string(run).c_str());

                strcat(mazename, ".txt");


                char runFilename[300] = {};
                void (*fn_eval)(Organism*,Environment*);
                sprintf(runFilename,"%s//objective_%d//%d//",filename, run,j);
                fn_eval = naive_evaluate_fitness;
                makeDirs(runFilename);
                p = maze_fitness_realtime(fn_eval,runFilename,mazename,useBorders,param);
                delete p;
            }
        }
    } else if( optimization_choice==2){

		int index_maze = 9;

		for(int run = index_maze;run<=index_maze;run++){

			for(int j=1;j<=50;j++){

                if (MAZE_TYPE == 0){
                    strcpy(mazename,"QDMazes/Mazes/maze");
                }
                else {
                    strcpy(mazename, "QDMazes/Sensitivity/maze");
                }

                strcat(mazename, std::to_string(run).c_str());

                strcat(mazename, ".txt");

                char runFilename[300] = {};
                void (*nov_eval)(noveltyarchive*,Organism*,Population*,Environment*,bool);
                sprintf(runFilename,"%s//novelty_15_%d//%d//",filename, run,j);
                nov_eval = naive_evaluate_individual;
                makeDirs(runFilename);
                //p = maze_novelty_realtime(runFilename,mazename,useBorders,param);
                p = maze_novelty_realtime(nov_eval,runFilename,mazename,useBorders,param);
				cout << "cleaning population" << endl;
				delete p;
            }

			char logsFilename[300] = {};

			sprintf(logsFilename,"%s//novelty_15_%d//%s",filename, run, "logging_moea.txt");

			NEAT::print_divergence_parameters(logsFilename);
        }

	} else if( optimization_choice==3){

        int index_maze = 9;

        for(int run = index_maze;run<=index_maze;run++){

            for(int j=1;j<=50;j++){

                if (MAZE_TYPE == 0){
                    strcpy(mazename,"QDMazes/Mazes/maze");
                }
                else {
                    strcpy(mazename, "QDMazes/Sensitivity/maze");
                }

                strcat(mazename, std::to_string(run).c_str());

                strcat(mazename, ".txt");

				char runFilename[300] = {};

				sprintf(runFilename,"%s//nss_0.4_2_%d//%d//",filename,run,j);
				makeDirs(runFilename);

				p = maze_novelty_surprise_realtime(runFilename,mazename,nearestNeighbors,useBorders, param);

				delete p;
			}

            char logsFilename[300] = {};

            sprintf(logsFilename,"%s//nss_0.4_2_%d//%s",filename,run, "logging_moea.txt");

            NEAT::print_divergence_parameters(logsFilename);
		}

    } else if(optimization_choice>=9) {

		int index_maze = 2;

        for(int run = index_maze;run<=index_maze;run++){

            cout << "num objectives " << num_objectives << endl;

            cout << "experiment " << NEAT::experiment << endl;

            for(int j=1;j<=50;j++){

                if (MAZE_TYPE == 0){
                    strcpy(mazename,"QDMazes/Mazes/maze");
                }
                else {
                    strcpy(mazename, "QDMazes/Sensitivity/maze");
                }

                strcat(mazename, std::to_string(run).c_str());

                strcat(mazename, ".txt");

                char runFilename[300] = {};
                sprintf(runFilename,"%s//moea_ns_%d//%d//",filename, run,j);

                makeDirs(runFilename);

                p = maze_novelty_surprise_realtime_multiobjective(runFilename,mazename,nearestNeighbors,useBorders,param);

                cout << "cleaning population" << endl;
                delete p;
            }

            char logsFilename[300] = {};

            sprintf(logsFilename,"%s//moea_ns_%d//%s",filename, run, "logging_moea.txt");

            NEAT::print_divergence_parameters(logsFilename);
        }
    } else {
        cout<<"Not an available optimization option."<<endl;
    }
}

int main(int argc, char **argv) {
	char mazename[100]="hard_maze.txt";
	char filename[100]=".//logs//";
	char settingsname[100]="maze.ne";

	NEAT::Population *p;

	//***********RANDOM SETUP***************//
	/* Seed the random-number generator with current time so that
		the numbers will be different every time we run.    */
	//srand( (unsigned)time( NULL )  + getpid());
	//srand(1);

	//Load in the params
	if(argc>1){ strcpy(settingsname,argv[1]); }

	NEAT::load_neat_params(settingsname,true);

	cout<<"loaded"<<endl;

	if((NEAT::maze == 1 || NEAT::maze == 2) && NEAT::timesteps != 400)
	{
		cout << "ERROR " << endl;
		exit(-1);
	}

	if((NEAT::maze == 7) && NEAT::timesteps != 500)
	{
		cout << "ERROR " << endl;
		exit(-1);
	}

	if((NEAT::maze == 8) && NEAT::timesteps != 1000)
	{
		cout << "ERROR " << endl;
		exit(-1);
	}
	
	int batchRunMaze = NEAT::maze;

	runExperiment(batchRunMaze, 2, 50, 1.f, 20, -1);

	return(0);
}
