#include "surprise_math.h"

#include "population.h"
#include "organism.h"
#include "species.h"
#include "noveltyitem.h"

#include "helpers.h"

#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <random>
#include <mutex>

#include "linreg.h"
#include <stdbool.h>
#include <math.h>

#include <chrono>
#include <cfloat>
using namespace chrono;

// -------------------------------------------------
// CLASS CONTENTS
// -------------------------------------------------

std::mutex mutex_log_centroids;
std::mutex mutex_log_pred_centroids;

void write_on_file (ofstream * centroid_savefile, vector<float> centroids_x, vector<float> centroids_y) {
    
    mutex_log_centroids.lock();
    
    for(int i = 0; i < centroids_x.size(); i++){
        *centroid_savefile << centroids_x[i] << " " << centroids_y[i] << " " << endl;
    }
    
    mutex_log_centroids.unlock();
    
}

void write_on_file_pred (ofstream * pred_centroid_savefile, vector< vector<float> > predictedcentroids) {
    
    mutex_log_pred_centroids.lock();
    
    int i = 0;
    for(auto & pred : predictedcentroids)
    {
        *pred_centroid_savefile << pred[0] << " " << pred[1] << " " << i << " " << endl; 
        i++;
    }
    
    mutex_log_pred_centroids.unlock();
    
}


KMeansClusteringModel::KMeansClusteringModel(int num_cluster, float history, int maxPast, int neigh):
            num_cluster(num_cluster), history(history), maxPast(maxPast), neigh(neigh)
{
    //sanity check

	cout << "k_means " << num_cluster << endl;

	cout << "surprise neigh" << neigh << endl;
    
    if(neigh > num_cluster)
    {
        cout << "ERROR: NEIGH > NUM CLUSTER !! " << "setting neigh = num_cluster" << endl;
        neigh = num_cluster;
    }
}

KMeansClusteringModel::~KMeansClusteringModel(){
    
    if(centroid_savefile){ centroid_savefile.close(); }
    if(pred_centroid_savefile){ pred_centroid_savefile.close(); }
    if(fitness_savefile) {fitness_savefile.close();}
    if(kmeans_savefile) {kmeans_savefile.close();}
    if(no_points_log) {no_points_log.close();}
    
    for (int i = 0; i < threads.size(); ++i) {
        threads[i].join();
    }
    
    if(time_log) {time_log.close();}
}

void KMeansClusteringModel::initLogs(char* output_dir){
        
	// initialize logs
	char cName[100];
	sprintf(cName,"%scentroids.txt",output_dir);
	centroid_savefile.open(cName);
        
        
        sprintf(cName, "%smodel.txt", output_dir);
	model_savefile.open(cName);
        
        
        model_savefile << "Kmeans clustering " << num_cluster;
        

        
        if(old)
        {
           model_savefile << " Linear Regression distance from line "; 
        }
        else
        {
            model_savefile << " new ";
        }
    
        if(genotype)
        {
            model_savefile << " genotype ";
        }
        else
        {
            model_savefile << " phenotype ";
        }
        
        if(history > 0)
        {
            model_savefile << " history " << history << " " << endl;
        }
        else
        {
             model_savefile << " no history "<< endl;
        }
        
        model_savefile << " neighborhood: " << neigh << endl;


        model_savefile.close();
        
        
}

void KMeansClusteringModel::assignMaze(Environment* env){ }

void KMeansClusteringModel::updateValues(NEAT::Population* pop)
{
    phenotypeUpdateValues(pop);
}

void KMeansClusteringModel::phenotypeUpdateValues(NEAT::Population* pop)
{

    vector<int> counts_cluster(num_cluster, 0);

    vector<float> centroids_x(num_cluster, 0);
    vector<float> centroids_y(num_cluster, 0);

    vector<float> best_centroids_x(num_cluster, 0);
    vector<float> best_centroids_y(num_cluster, 0);

    vector<float> old_centroids_x(num_cluster, 0);
    vector<float> old_centroids_y(num_cluster, 0);

    vector<float> temp_centroids_x(num_cluster, 0);
    vector<float> temp_centroids_y(num_cluster, 0);

    double error, old_error = 0;

    vector<float> xcoordinates;
    vector<float> ycoordinates;

    vector<int> labels(pop->enumerateSize(), 0);

    vector<int> best_labels(pop->enumerateSize(), 0);

    high_resolution_clock::time_point t1;
    high_resolution_clock::time_point t2;

    if(time_log){
        t1 = high_resolution_clock::now();
    }

    /*****************
    ** prediction phase
    *****************/

    if(prev_centroids.size() >= 2*num_cluster)
    {
        vector< vector<float> > centroids_to_predict;

        if(history < 0){
            predictedcentroids.clear();
            flag_predictedcentroids.clear();
        }

        for(int i = 0; i < num_cluster; i++)
        {
            centroids_to_predict.clear();

            centroids_to_predict.emplace_back(prev_centroids[prev_centroids.size() - num_cluster*2 + i]);
            centroids_to_predict.emplace_back(prev_centroids[prev_centroids.size() - num_cluster + i]);
            predictedcentroids.emplace_back( predictNextPoint(centroids_to_predict));

            if(flag_prev_centroids[flag_prev_centroids.size() - num_cluster*2 + i] && flag_prev_centroids[flag_prev_centroids.size() - num_cluster + i])
            {
                flag_predictedcentroids.push_back(true);
            }
            else
            {
                flag_predictedcentroids.push_back(false);

            }
        }

        if(pred_centroid_savefile){

            std::thread t(write_on_file_pred, &pred_centroid_savefile, predictedcentroids);

            threads.push_back(std::move(t));
        }
    }
    else
    {
        if(pred_centroid_savefile){
            pred_centroid_savefile << endl;
        }
    }

    /*
     * Kmeans phase
     */

    double xpoint, ypoint;


    for(int i = 0; i < pop->species.size(); i++) {
        for(int j = 0;j < pop->species[i]->organisms.size(); j++) {
            xpoint = pop->species[i]->organisms[j]->noveltypoint->data[0][0];
            ypoint = pop->species[i]->organisms[j]->noveltypoint->data[0][1];

            xcoordinates.emplace_back(xpoint);
            ycoordinates.emplace_back(ypoint);
        }
    }

    vector<float> currentcentroid = calculateCentroid(xcoordinates, ycoordinates);

    /*subtract mean from current robots*/
    for(int h = 0;  h < xcoordinates.size(); h++)
    {
        xcoordinates[h] -= currentcentroid[0];
        ycoordinates[h] -= currentcentroid[1];
    }

    if(prev_centroids.size() >= num_cluster)
    {

        /*seed from previous generation kmeans*/
        for(int i = 0; i < num_cluster; i++)
        {
            centroids_x[i] = prev_centroids[prev_centroids.size() - num_cluster + i][0];
            centroids_y[i] = prev_centroids[prev_centroids.size() - num_cluster + i][1];



            /*subtract mean*/
            centroids_x[i] -= currentcentroid[0];
            centroids_y[i] -= currentcentroid[1];
        }


    }
    else
    {
        /*gen = 0 -> random initialization of centroids*/

        vector<int> indexes(xcoordinates.size(), 0);

        for(int i = 0; i < xcoordinates.size(); i++)
        {
            indexes[i] = i;
        }

        random_shuffle ( indexes.begin(), indexes.end() );

        for(int i = 0; i < num_cluster; i++)
        {
            centroids_x[i] = xcoordinates[indexes[i]];
            centroids_y[i] = ycoordinates[indexes[i]];


        }


    }

    int iter_counter = 0;
    double squared_norm = 0.0;
    float best_error = numeric_limits<float>::max();
    error = 0.0;


    //k_means_log << "begin loop" << endl;

    vector<bool> points_taken(xcoordinates.size(), false);
    vector<int> centroids_not_taken;

    do {
        /* save error from last step */
        old_error = error;
        error = 0.0;
        squared_norm = 0.0;

        //k_means_log << "iter " << iter_counter << endl;

        /* clear old counts and temp centroids */
        for (int i = 0; i < num_cluster; i++) {

            counts_cluster[i] = 0;

            temp_centroids_x[i] = 0;
            temp_centroids_y[i] = 0;

        }

        /*safe check- > all points assigned*/
        std::fill(labels.begin(), labels.end(), -1);

        for (int h = 0; h < xcoordinates.size(); h++) {
            /* identify the closest cluster to the point h */
            double min_distance = numeric_limits<double>::max();
            double dist = 0.0;

            for (int i = 0; i < num_cluster; i++) {

                dist = distance(xcoordinates[h], ycoordinates[h], centroids_x[i], centroids_y[i]);

                if (dist < min_distance) {
                    labels[h] = i;
                    min_distance = dist;
                }
            }
            /* update size and temp centroid of the destination cluster */

            temp_centroids_x[labels[h]] += xcoordinates[h];
            temp_centroids_y[labels[h]] += ycoordinates[h];


            counts_cluster[labels[h]]++;
            /* update standard error */
            error += min_distance;
        }

        for (int i = 0; i < num_cluster; i++) { /* update all centroids */

            old_centroids_x[i] = centroids_x[i];
            old_centroids_y[i] = centroids_y[i];

            if(counts_cluster[i])
            {
                centroids_x[i] = (float) temp_centroids_x[i] / counts_cluster[i];
                centroids_y[i] = (float) temp_centroids_y[i] / counts_cluster[i];


            }
        }



        for(int i = 0; i < num_cluster; i++)
        {
            squared_norm += (centroids_x[i] - old_centroids_x[i])*(centroids_x[i] - old_centroids_x[i]);
            squared_norm += (centroids_y[i] - old_centroids_y[i])*(centroids_y[i] - old_centroids_y[i]);
        }

        if(error < best_error){
            for (int i = 0; i < num_cluster; i++)
            {
                best_centroids_x[i] = centroids_x[i];
                best_centroids_y[i] = centroids_y[i];
            }

            for(int i = 0; i < labels.size(); i++)
            {
                best_labels[i] = labels[i];
            }

            best_error = error;
        }

        squared_norm = sqrt(squared_norm);

        if(kmeans_savefile){
            kmeans_savefile << iter_counter << ": " << error << endl;
        }

        iter_counter++;

    } while (squared_norm > FLT_EPSILON && iter_counter < 300);

    if(k_means_log){
        k_means_log << "end_loop" << endl;
    }

    if(kmeans_savefile){
        kmeans_savefile <<  endl;
    }

    int no_points = 0;

    vector<int> cluster(num_cluster, 0);

    vector<float> centr_x(num_cluster, 0.0);
    vector<float> centr_y(num_cluster, 0.0);

    vector<int> num(num_cluster, 0.0);

    for(int i = 0; i < best_labels.size(); i++)
    {
        cluster[best_labels[i]] = 1;
        if(k_means_log)
        {
            k_means_log << "point " << i << " " << xcoordinates[i] + currentcentroid[0]
                        << " " << ycoordinates[i] + currentcentroid[1] << " label " << best_labels[i] << endl;

            centr_x[best_labels[i]] += xcoordinates[i] + currentcentroid[0];
            centr_y[best_labels[i]] += ycoordinates[i] + currentcentroid[1];
            num[best_labels[i]]++;
        }
    }

    if(k_means_log){
        for(int i = 0; i < num_cluster; i++){
            centr_x[i] /= num[i];
            centr_y[i] /= num[i];
            k_means_log << "cluster debug " << i << " " << centr_x[i] << " " <<
                        centr_y[i] << endl;
        }
    }

    for(int i = 0; i < cluster.size(); i++)
    {
        if(!cluster[i]){
            no_points++;
            flag_prev_centroids.push_back(false);
        }
        else
        {
            flag_prev_centroids.push_back(true);
        }
    }

    for(int i = 0; i < num_cluster; i++)
    {

        best_centroids_x[i] += currentcentroid[0];
        best_centroids_y[i] += currentcentroid[1];

        prev_centroids.emplace_back(vector<float>() = {best_centroids_x[i], best_centroids_y[i]});

    }

    if(no_points_log)
    {
        no_points_log << no_points << endl;
    }

    if(centroid_savefile)
    {

        threads.push_back(std::move(std::thread(write_on_file, &centroid_savefile, best_centroids_x, best_centroids_y)));
    }

    //erase old centroids for performance reason (case k too high)
    if(prev_centroids.size() > num_cluster*2){
        prev_centroids.erase(prev_centroids.begin(),
                             prev_centroids.begin() + prev_centroids.size() - num_cluster*2);

        flag_prev_centroids.erase(flag_prev_centroids.begin(),
                                  flag_prev_centroids.begin() + flag_prev_centroids.size() - num_cluster*2);
    }

    if(time_log){

        t2 = high_resolution_clock::now();

        time_log << (double)duration_cast<duration<double>>(t2 - t1).count() << endl;

    }
}



float KMeansClusteringModel::evaluateIndividual(vector<float> &position){
    
    double dist = 0.0; 
    double min_dist = numeric_limits<double>::max();
    
    if(prev_centroids.size() >= 2*num_cluster && predictedcentroids.size() >= num_cluster){	// sanity for predictedcentroid
        
        //no history
        if(history < 0){
            
            if(neigh <= 1){ //no neighborhood
                
                int index = 0;
				int saved_index = 0;
                for(vector <vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++)
                {

                        dist = distance(position[0], position[1], (*it)[0], (*it)[1]); 

                        if(dist < min_dist)
                        {
                            min_dist = dist;

							saved_index = index;
                        }
                        

                    index++;
                }

				map_prediction_individuals[saved_index]++;

                return min_dist;
            }
            else //yes neighborhood
            {
                vector<float> distances;
                int index = 0;

                for(vector <vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++)
                {

                        dist = distance(position[0], position[1], (*it)[0], (*it)[1]); 
                        distances.emplace_back(dist);

                    index++;
                }

                sort(distances.begin(), distances.end());

                dist = 0.0;

                for(int i = 0; i < neigh; i++)
                {
                    dist += distances[i];
                }

                return (float)dist/neigh;
            }
        }
        else //yes history
        {
            int pred_centroids_size = predictedcentroids.size();
            int index_prediction = 0;
            
            //as vanilla k means prediction we find the closest predicted cluster centroid 
            for(int i = 0; i < num_cluster; i++)
            {
                dist = distance(position[0], position[1], predictedcentroids[pred_centroids_size - num_cluster + i][0], predictedcentroids[pred_centroids_size - num_cluster + i][1]); 
                if(dist < min_dist)
                {
                    index_prediction = i;
                    min_dist = dist;
                }
            }
            
            
            double weight = 0;
            double sum_weight = 0;
            
            double result = 0;
            
            if(maxPast == -1 ){
            
                ///we take past prediction history of the cluster at the index_prediction with weight history
                for(int i = 0; i < (int)pred_centroids_size/num_cluster; i++)
                {
                     weight = pow(history,i);
                     sum_weight += weight;
                     dist = weight*distance(position[0], position[1], predictedcentroids[index_prediction + i*num_cluster][0], predictedcentroids[index_prediction + i*num_cluster][1]); 
                     result += dist;
                }
            }
            else
            {
                int past = maxPast;
                
                if(maxPast >= (int)pred_centroids_size/num_cluster)
                {
                    past = (int)pred_centroids_size/num_cluster;
                }
                
                for(int i = (int)pred_centroids_size/num_cluster - 1; i >= (int)pred_centroids_size/num_cluster - past; i--)
                {
                     weight = pow(history,i);
                     sum_weight += weight;
                     dist = weight*distance(position[0], position[1], predictedcentroids[index_prediction + i*num_cluster][0], predictedcentroids[index_prediction + i*num_cluster][1]); 
                     result += dist;
                }
            }
            
            return (float)result/sum_weight;
        }
    }
    else
    {
        return 1;
    }
}

float KMeansClusteringModel::evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop){
	return evaluateIndividual(org->noveltypoint->data[0]);
}

float KMeansClusteringModel::evaluateIndividualWithArchive(NEAT::Organism* org, NEAT::Population* pop, noveltyarchive* nov_archive){

    double dist = 0.0;
    double min_dist = numeric_limits<double>::max();

    vector<float> position = org->noveltypoint->data[0];

    if(prev_centroids.size() >= 2*num_cluster && predictedcentroids.size() >= num_cluster){	// sanity for predictedcentroid

        //no history
        if(history < 0){

            if(neigh <= 1){ //no neighborhood

                int index = 0;
                int saved_index = 0;
                for(vector <vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++)
                {


                    dist = distance(position[0], position[1], (*it)[0], (*it)[1]);
                   
                    if(dist < min_dist)
                    {
                        min_dist = dist;

                        saved_index = index;
                    }
                   

                    index++;
                }

                map_prediction_individuals[saved_index]++;

                return min_dist;
            }
            else //yes neighborhood
            {
                vector<float> distances = nov_archive->get_distances(org->noveltypoint);
                int index = 0;

                for(vector <vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++) {

                    dist = distance(position[0], position[1], (*it)[0], (*it)[1]);
                    distances.emplace_back(dist);

                    index++;
                }


                sort(distances.begin(), distances.end());

                dist = 0.0;

                for(int i = 0; i < neigh; i++)
                {
                    dist += distances[i];
                }

                return (float)dist/neigh;
            }
        }
        else //yes history
        {
            int pred_centroids_size = predictedcentroids.size();
            int index_prediction = 0;

            //as vanilla k means prediction we find the closest predicted cluster centroid
            for(int i = 0; i < num_cluster; i++)
            {
                dist = distance(position[0], position[1], predictedcentroids[pred_centroids_size - num_cluster + i][0], predictedcentroids[pred_centroids_size - num_cluster + i][1]);
                if(dist < min_dist)
                {
                    index_prediction = i;
                    min_dist = dist;
                }
            }


            double weight = 0;
            double sum_weight = 0;

            double result = 0;

            if(maxPast == -1 ){

                ///we take past prediction history of the cluster at the index_prediction with weight history
                for(int i = 0; i < (int)pred_centroids_size/num_cluster; i++)
                {
                    weight = pow(history,i);
                    sum_weight += weight;
                    dist = weight*distance(position[0], position[1], predictedcentroids[index_prediction + i*num_cluster][0], predictedcentroids[index_prediction + i*num_cluster][1]);
                    result += dist;
                }
            }
            else
            {
                int past = maxPast;

                if(maxPast >= (int)pred_centroids_size/num_cluster)
                {
                    past = (int)pred_centroids_size/num_cluster;
                }

                for(int i = (int)pred_centroids_size/num_cluster - 1; i >= (int)pred_centroids_size/num_cluster - past; i--)
                {
                    weight = pow(history,i);
                    sum_weight += weight;
                    dist = weight*distance(position[0], position[1], predictedcentroids[index_prediction + i*num_cluster][0], predictedcentroids[index_prediction + i*num_cluster][1]);
                    result += dist;
                }
            }

            return (float)result/sum_weight;
        }
    }
    else
    {
        return 1;
    }

}

float KMeansClusteringModel::evaluateLocalPredictionCompetition(NEAT::Organism *org, Population *pop) {

	double dist = 0.0;
	double min_dist = numeric_limits<double>::max();

	vector<int> labels(pop->organisms.size(), 0);

	vector<noveltyitem*> locals;



	int index = 0;
	int saved_index = 0;

	if(prev_centroids.size() >= 2*num_cluster && predictedcentroids.size() >= num_cluster) {    // sanity for predictedcentroid

		for (vector<vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++) {

			dist = distance(org->noveltypoint->data[0][0], org->noveltypoint->data[0][1], (*it)[0], (*it)[1]);
			//cout << "dist: " << dist << endl;
			if(dist < min_dist)
			{
				min_dist = dist;

				saved_index = index;
			}

			index++;

		}

		for (int h = 0; h < (int)pop->organisms.size(); h++) {
			/* identify the closest cluster to the point h */
			dist = 0.0;
			min_dist = numeric_limits<double>::max();

			index = 0;

			for (vector<vector<float> >::iterator it = predictedcentroids.begin(); it != predictedcentroids.end(); it++) {

				dist = distance(pop->organisms[h]->noveltypoint->data[0][0], pop->organisms[h]->noveltypoint->data[0][1], (*it)[0], (*it)[1]);

				if (dist < min_dist) {
					labels[h] = index;
					min_dist = dist;
				}

				index++;
			}

			if(labels[h] == saved_index)
			{
				locals.emplace_back(pop->organisms[h]->noveltypoint);
			}
		}


		float loc_fit;

		float sum = 0.0;

		for(int h=0; h < locals.size(); ++h)
		{
			loc_fit = locals[h]->fitness;

			if (loc_fit < org->noveltypoint->fitness)
			{
				sum += 1;
			}
		}

		if(locals.size() != 0)
		{
			return (float)(sum/locals.size());

		} else{
			return 0;
		}
	}

	return 1;


}

void KMeansClusteringModel::saveFitness(float novelty, float surprise)
{
    if(fitness_savefile){ fitness_savefile << novelty << " " << surprise << " " << endl; }
}

void KMeansClusteringModel::saveFitness(float novelty, float surprise, float objective){
    if(fitness_savefile){ fitness_savefile << novelty << " " << surprise << " " << " " << objective << endl; }
}
    
   







// -------------------------------------------------
// BASIC MATH OPERATORS
// -------------------------------------------------

vector<float> predictNextPoint(float x1, float y1, float x2, float y2)
{
    vector< vector<float> > points;
    
    vector<float> temp1, temp2;
    
    temp1.push_back(x1);
    temp1.push_back(y1);
    
    temp2.push_back(x2);
    temp2.push_back(y2);
    
    points.push_back(temp1);
    points.push_back(temp2);
    
    return predictNextPoint(points);
}

vector<float> predictNextPoint(vector<vector<float> > &prevPoints){
	vector<float> nextPoint;
	// find previous two points
	if(prevPoints.size()<2){
		cout << "Cannot calculate next point, too few data" << endl;
		return nextPoint;
	}
	/*
	float curr_x = prevPoints[prevPoints.size()-1][0];
	float curr_y = prevPoints[prevPoints.size()-1][1];
	float prev_x = prevPoints[prevPoints.size()-2][0];
	float prev_y = prevPoints[prevPoints.size()-2][1];
	*/
	float curr_x = prevPoints[prevPoints.size()-1][0];
	float curr_y = prevPoints[prevPoints.size()-1][1];
	float prev_x = prevPoints[prevPoints.size()-2][0];
	float prev_y = prevPoints[prevPoints.size()-2][1];
	
	// direction (basic vector math), i.e. prev_x+d_x = curr_x
	float d_x = curr_x-prev_x;
	float d_y = curr_y-prev_y;
	
	float next_x = curr_x + d_x;
	float next_y = curr_y + d_y;
	
	nextPoint.push_back(next_x);
	nextPoint.push_back(next_y);
}

vector<float> predictNextPoint(vector<float>& line_params, vector< vector<float> >& prev_centroids)
{
    float avg_distance = 0;
    float angle;
    vector< vector <float> > proj_centroids;
    vector<float> result;
    
    for(int i = 0; i < 5; i++)
    {
        proj_centroids[i] = prev_centroids[i];
        proj_centroids[i][0] = -1/(line_params[0]*line_params[0]) * prev_centroids[i][0];
        proj_centroids[i][1] = -1/line_params[0]*prev_centroids[i][0] + line_params[1];
    }
    
    for(int i = 1; i < 5; i++)
    {
        avg_distance += distance(proj_centroids[i][0], proj_centroids[i][1], proj_centroids[i-1][0], proj_centroids[i-1][1]);
    }
    
    avg_distance /= 5;
    
    angle = atan2(proj_centroids[1][1] - proj_centroids[0][1], proj_centroids[1][0] - proj_centroids[0][0]);
    
    result[0] = proj_centroids[proj_centroids.size() - 1][0] + avg_distance*cos(angle);
    result[1] = proj_centroids[proj_centroids.size() - 1][1] + avg_distance*sin(angle);
    
    return result;
}

vector<vector<float> >  getHalfPop(vector<float> &maze_start, vector<float> &xcoordinates, vector<float> &ycoordinates){
	int size = xcoordinates.size();
	float distances[size];
	vector<float> toSort_distances;;
	vector<vector<float> > halfpop;
	//cout << "size" << size << endl;
	for(int i =0; i < size; i++){
		distances[i]  = distance(maze_start[0], maze_start[1], xcoordinates[i], ycoordinates[i]);
		toSort_distances.push_back(distances[i]);
	}
	std::sort (toSort_distances.begin(), toSort_distances.end());  
	// median distance;
	float median_distance = toSort_distances[floor(toSort_distances.size()/2)];
	
	for(int i = 0; i < size; i++){
		if(distances[i]>=median_distance){
			vector<float> added_point;
			added_point.push_back(xcoordinates[i]);
			added_point.push_back(ycoordinates[i]);
			halfpop.push_back(added_point);
		}
	}
	//cout << "half population is " << halfpop.size() << " out of " << size;
	return halfpop;
}

vector<float> calculateMedoid(vector<float> &xcoordinates, vector<float> &ycoordinates){
	double dist = 0;
	float x_medoid = 0;
	float y_medoid = 0;
	double smallest_dist = 100000000000000000.00;
	for (int i = 0; i< xcoordinates.size(); i++){
		for(int j=0; j < xcoordinates.size(); j++){
			dist += distance(xcoordinates[i], ycoordinates[i],xcoordinates[j],ycoordinates[j]);
		}
		if (dist < smallest_dist){
			smallest_dist = dist;
			x_medoid = xcoordinates[i];
			y_medoid = ycoordinates[i];
		}
		dist = 0;
	}

	vector<float> medoid;
	medoid.push_back(x_medoid);
	medoid.push_back(y_medoid);
	return medoid;
}
vector<float> calculateMedoid(vector<vector<float> > &points){
	double dist;
	float x_medoid = 0;
	float y_medoid = 0;
	double smallest_dist = 100000000000000000.00;
	for (int i = 0; i< points.size(); i++){
		dist = 0;
		for(int j=0; j < points.size(); j++){
			dist += distance(points[i][0],points[i][1],points[j][0],points[j][1]);
		}
		if (dist < smallest_dist){
			smallest_dist = dist;
			x_medoid = points[i][0];
			y_medoid = points[i][1];
		}
	}

	vector<float> medoid;
	medoid.push_back(x_medoid);
	medoid.push_back(y_medoid);
	return medoid;
}

vector<float> calculateCentroid(vector<vector<float> > &points){
	float txpoints = 0;
	float typoints = 0;
	for (int counter = 0; counter < points.size(); counter++){
		txpoints += points[counter][0];
		typoints += points[counter][1];
	}
	vector<float> centroid;
	centroid.push_back(txpoints / points.size());
	centroid.push_back(typoints / points.size());
	return centroid;
}
vector<float> calculateCentroid(vector<float> &xcoordinates, vector<float> &ycoordinates){
	float txpoints = 0;
	float typoints = 0;
	for (int counter = 0; counter < xcoordinates.size(); counter++){
		txpoints += xcoordinates[counter];
		typoints += ycoordinates[counter];
	}
	vector<float> centroid;
	centroid.push_back(txpoints / xcoordinates.size());
	centroid.push_back(typoints / ycoordinates.size());
	return centroid;
}

vector<vector<float> > two_means(vector<float>&xcoordinates, vector<float>&ycoordinates, int gen_no, double last_centroid_1_x, double last_centroid_1_y, double last_centroid_2_x, double last_centroid_2_y){
	vector<float> cluster1x;
	vector<float> cluster1y;
	vector<float> cluster2x;
	vector<float> cluster2y;
	vector<vector<float> > cluster_centroids;
	float tcluster1x = 0;
	float tcluster1y = 0;
	float tcluster2x = 0;
	float tcluster2y= 0;
	vector<float> centroid_c1;
	vector<float> centroid_c2;
	float cx1;
	float cy1;
	float cx2;
	float cy2;
	float previous_cx1 = centroid_c1[0];
	float previous_cy1 = centroid_c1[1];
	float previous_cx2 = centroid_c2[0];
	float previous_cy2 = centroid_c2[1];

	if(gen_no == 0){	// NOTE: BOGUS VALUES, perhaps take environment starting position?
		cx1 = 24;
		cy1 = 78;
		cx2 = 34;
		cy2 = 47;
	}

	if(gen_no > 0){
		cx1 = last_centroid_1_x;
		cy1 = last_centroid_1_y;
		cx2 = last_centroid_2_x;
		cy2 = last_centroid_2_y;

	}
	
	float pcx1 = 0;
	float pcx2 = 0;
	float pcy1 = 0;
	float pcy2 = 0;

	while(floor(pcx1) != floor(cx1) && floor(pcx2) != floor(cx2) && floor(pcy1) != floor(cy1) && floor(pcy2) != floor(cy2)){
		pcx1 = cx1;
		pcx2 = cx2;
		pcy1 = cy1;
		pcy2 = cy2;

		for(int i = 0; i< xcoordinates.size();i++){
			double d1 = distance(cx1, cy1, xcoordinates[i], ycoordinates[i]);
			double d2 = distance(cx2, cy2, xcoordinates[i], ycoordinates[i]);
			if(d1 < d2){
				cluster1x.push_back(xcoordinates[i]);
				cluster1y.push_back(ycoordinates[i]);
			}
			else if(d2 < d1){
				cluster2x.push_back(xcoordinates[i]);
				cluster2y.push_back(ycoordinates[i]);
			}
		}

		for(int counter =0; counter < cluster1x.size(); counter++){
			tcluster1x += cluster1x[counter];
			tcluster1y += cluster1y[counter];
		}

		for(int counter = 0; counter < cluster2x.size();counter++){
			tcluster2x += cluster2x[counter];
			tcluster2y += cluster2y[counter];
		}

		cx1 = tcluster1x / cluster1x.size();
		cy1 = tcluster1y / cluster1y.size();
		cx2 = tcluster2x / cluster2x.size();
		cy2 = tcluster2y / cluster2y.size();
		
		tcluster1x = 0;
		tcluster1y = 0;
		tcluster2x = 0;
		tcluster2y = 0;
	}
	centroid_c1.push_back(cx1);
	centroid_c1.push_back(cy1);
	centroid_c2.push_back(cx2);
	centroid_c2.push_back(cy2);
	cluster_centroids.push_back(centroid_c1);
	cluster_centroids.push_back(centroid_c2);

	centroid_c1.clear();
	centroid_c2.clear();

	return cluster_centroids;
}

vector<float> BatchGradientDescent(vector< vector<float> >& centroids){
    int nsamples = centroids.size();
    int max_iterations = 100;
    
    vector<float> result;
    
    //add X0 = 1.
    double featureA, featureB, tempA, tempB;
    
    featureA = 0;
    featureB = 0;
    tempA = 1.0;
    tempB = 1.0;
    double sumA = 0.0;
    double sumB = 0.0;
    
    double prediction = 0.0;
    
    double cost = 0.0;
    
    double lrate = 0.0001;
    
    int num_centroids = 100;
    
    if(num_centroids > nsamples)
    {
        num_centroids = nsamples;
    }
    
    for(int j  = 0; j < max_iterations; j++){
        
        sumA = 0.0;
        sumB = 0.0;
    
        for(int i = nsamples - 1; i > nsamples - num_centroids && i >= 0; i--)
        {   
            prediction = featureA*centroids[i][0] + featureB;
            
            sumA += (prediction - centroids[i][1])*centroids[i][0];
            sumB += prediction - centroids[i][1];
        }
        
        featureA = featureA - (double)lrate/num_centroids*sumA;
        featureB = featureB - (double)lrate/num_centroids*sumB;
        
        /*cost = 0.0;
        
        for(int i = nsamples - 1; i > nsamples - 10 && i >= 0; i--)
        {  
          cost += (featureA*centroids[i][0] + featureB - centroids[i][1])*(featureA*centroids[i][0] + featureB - centroids[i][1]);
        }*/
        
    }
    
    //cout << "cost " << (double)1/10*cost << endl;
    
    //cout << featureA << " " << featureB << endl;
    
    result.push_back(featureA);
    
    result.push_back(featureB);
    
    return result;
}

//connect two groups of centroids based on genotype's similarity measure
void ConnectCentroids(map<int, vector<NEAT::Genome> > &centroids_t_1, map<int, vector<NEAT::Genome> > &centroids_t_2, vector<int>& indexes_to_use)
{
    bool finished;
	
    for(int i = 0; i < indexes_to_use.size(); i++)
    {
            indexes_to_use[i] = i;
    }

     do{
        finished = true;

        for(int i = 0; i < centroids_t_1.size(); i++){

            for(int j = 1; j < indexes_to_use.size(); j++){

                if(sum_similarity(centroids_t_1[i], centroids_t_2[indexes_to_use[i]]) + sum_similarity(centroids_t_1[j], centroids_t_2[indexes_to_use[j]])
                   <     
                   sum_similarity(centroids_t_1[i], centroids_t_2[indexes_to_use[j]]) + sum_similarity(centroids_t_1[j], centroids_t_2[indexes_to_use[i]]) )
                {
                    finished = false;

                    //XOR swap
                    indexes_to_use[i] = indexes_to_use[i] ^ indexes_to_use[j];
                    indexes_to_use[j] = indexes_to_use[i] ^ indexes_to_use[j];
                    indexes_to_use[i] = indexes_to_use[i] ^ indexes_to_use[j];
                }

            }
        }


    }while(!finished);
	
}

double sum_similarity(vector<NEAT::Genome> & centroids_1, vector<NEAT::Genome> & centroids_2)
{
	double result = 0.0;
	
	for(int i = 0; i < centroids_1.size(); i++)
	{
		for(int j = 0; j < centroids_2.size(); j++){
			result += centroids_1[i].compatibility(&centroids_2[j]);
		}
	}
	
	return result;
}

vector<int> DBSCAN(double eps, unsigned int minPts, vector< vector<float> > & points, int *num_cluster){
    
    int index_cluster = -1;
    
    vector<int> label_cluster(points.size(), -1);
    
    vector<bool> visited(points.size(), false);
    
    vector<bool> noise(points.size(), false);
    
    vector< int > neighborhood, neighborhood_prime;
    
    for(int i = 0; i < points.size(); i++)
    {
        if(!visited[i])
        {
            visited[i] = true;
            
            neighborhood = regionQuery(i, points, eps);
            
            if(neighborhood.size() < minPts)
            {
                noise[i] = true;
            }
            else
            {
                index_cluster++;
                
                label_cluster[i] = index_cluster;
                
                for(int j = 0; j < neighborhood.size(); j++)
                {
                    if(!visited[neighborhood[j]]){
                        
                        visited[neighborhood[j]] = true;
                        
                        neighborhood_prime = regionQuery(neighborhood[j], points, eps);
                        
                        if(neighborhood_prime.size() >= minPts)
                        {
                             neighborhood.insert(neighborhood.end(), neighborhood_prime.begin(), neighborhood_prime.end());
                        }
                        
                    }
                    
                    if(label_cluster[neighborhood[j]] == -1)
                    {
                        label_cluster[neighborhood[j]] = index_cluster;
                    }
                }
                
            }
        }      
    }
    
    *num_cluster = index_cluster + 1;
    
    return label_cluster;
}

vector<int> regionQuery(int index_point, vector< vector<float> > & points, double eps){
    
    vector<int> neighborhood;
    
    for(int i = 0; i < points.size(); i++)
    {
        if(distance(points[i][0], points[i][1], points[index_point][0], points[index_point][1]) <= eps)
        {
            neighborhood.push_back(i);
        }
    }
    
    return neighborhood;
    
}

bool polynomialfit(int obs, int degree,
		   double *dx, double *dy, double *store) /* n, p */
{

}

double distance_from_line(const vector<float> &point, const vector<float>& line_param)
{
    double d = point[1] - line_param[1]*point[0] - line_param[0];
        
    if(d < 0)
        d = -d;

    double den = sqrt(1 + line_param[1]*line_param[1]);

    return d/den;
}
