#ifndef SURPRISE_MATH_H
#define	SURPRISE_MATH_H

#include <vector>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>

#include "population.h"
#include "organism.h"
#include "genome.h"
#include "noveltyset.h"

class Environment;

class AbstractSurpriseModel {
    protected:
        // no variables it seems
    public:
        //SurpriseHistory();
        virtual ~AbstractSurpriseModel() = 0;
        //virtual void predictNextValues() = 0;
        //virtual void clearValues() = 0;
        virtual void initLogs(char* output_dir) = 0;
        virtual void assignMaze(Environment* env) = 0;
        virtual void updateValues(NEAT::Population* pop) = 0;
        virtual float evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop = NULL) = 0;
        virtual float evaluateIndividual(std::vector<float> &position) = 0;

        virtual float evaluateIndividualWithArchive(NEAT::Organism* org, NEAT::Population* pop, noveltyarchive* archive){
            return 1.0f;
        }

        virtual float evaluateLocalPredictionCompetition(NEAT::Organism* org, NEAT::Population* pop = NULL)
        {
            return 1.0;
        }
        
        virtual void saveFitness(float novelty, float surprise)
        {
        }
        
        virtual void saveFitness(float novelty, float surprise, float objective)
        {   
        }
};

inline AbstractSurpriseModel::~AbstractSurpriseModel() { }

class KMeansClusteringModel : public AbstractSurpriseModel {
    protected:
        ofstream model_savefile, centroid_savefile,pred_centroid_savefile, fitness_savefile, kmeans_savefile, no_points_log, k_means_log, time_log;
	vector<vector<float> > prev_centroids;
        vector<bool > flag_prev_centroids;
        vector<vector<float> > prev_centroids_t_2;
        vector<thread> threads;
        vector<int> count_cluster_t_1;
        vector<int> count_cluster_t_2;
        vector< vector<float> > points_t1;
        vector< vector<float> > points_t2;
        vector< int > label_t_1;
        vector< int > label_t_2;
        int num_cluster;
        vector<vector<float> > predictedcentroids;
        vector<bool > flag_predictedcentroids;
        map<int, vector<NEAT::Genome> > map_centroids_genome_t_1;
        map<int, vector<NEAT::Genome> > map_centroids_genome_t_2;
        map<int, int > map_prediction_individuals;
        bool old, genotype;
        float history;
        int maxPast, neigh;
        
    public:
        KMeansClusteringModel(int num_cluster = 2, float history = -1.0, int maxPast = -1, int neigh = 1);
        ~KMeansClusteringModel();
        void initLogs(char* output_dir);
        void assignMaze(Environment* env);
        void updateValues(NEAT::Population* pop);
        void KMedoids(NEAT::Population* pop);
        void phenotypeUpdateValues(NEAT::Population* pop);
        void LinearRegressionUpdateValues(NEAT::Population* pop);
        float evaluateIndividual(NEAT::Organism* org, NEAT::Population* pop = NULL);
        float evaluateIndividualWithArchive(NEAT::Organism* org, NEAT::Population* pop, noveltyarchive* archive);
        float evaluateIndividual(std::vector<float> &position);
        float evaluateLocalPredictionCompetition(NEAT::Organism* org, NEAT::Population* pop = NULL);
        void saveFitness(float novelty, float surprise);
        void saveFitness(float novelty, float surprise, float objective);
};

std::vector<float> predictNextPoint(std::vector<std::vector<float> > &points);
std::vector<float> predictNextPoint(float x1, float y1, float x2, float y2);
std::vector<std::vector<float> >  getHalfPop(std::vector<float> &maze_start, std::vector<float> &xcoordinates, std::vector<float> &ycoordinates);
std::vector<float> calculateMedoid(std::vector<float> &xcoordinates, std::vector<float> &ycoordinates);
std::vector<float> calculateMedoid(std::vector<std::vector<float> > &points);
std::vector<float> calculateCentroid(std::vector<float> &xcoordinates, std::vector<float> &ycoordinates);
std::vector<double> calculateCentroid2(std::vector<double> &xcoordinates, std::vector<double> &ycoordinates);
std::vector<float> calculateCentroid(std::vector<std::vector<float> > &points);
std::vector<std::vector<float> > two_means(std::vector<float>&xcoordinates, std::vector<float>&ycoordinates, int gen_no, float last_centroid_1_x, float last_centroid_1_y, float last_centroid_2_x, float last_centroid_2_y);
std::vector<float> BatchGradientDescent(std::vector< std::vector<float> > &centroids);
void ConnectCentroids(std::map<int, std::vector<NEAT::Genome> > &centroids_t_1, std::map<int, std::vector<NEAT::Genome> > &centroids_t_2, std::vector<int>& indexes_to_use);
double sum_similarity(vector<NEAT::Genome> & centroids_1, vector<NEAT::Genome> & centroids_2);

vector<int> DBSCAN(double eps, unsigned int minPts, vector< vector<float> > & points, int *num_cluster);

vector<int> regionQuery(int index_point, vector< vector<float> > & points, double eps);

bool polynomialfit(int obs, int degree, double *dx, double *dy, double *store); /* n, p */

double distance_from_line(const vector<float> &point, const vector<float>& line_param);

#endif	/* SURPRISE_MATH_H */

