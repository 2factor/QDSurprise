//
// Created by idgresearch on 18/04/17.
//

#ifndef MAZESURPRISESEARCH_TESTS_NSGAII_H
#define MAZESURPRISESEARCH_TESTS_NSGAII_H

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct org{
    float rank_fitness;
    float crowd_distance;
};

typedef struct org org;

int test_sorting()
{
    org o = {1, 0.5};
    org o2 = {2, 0.2};
    org o3 = {1, 0.5};

    vector<org*> organisms = {&o, &o2, &o3};

    std::sort(organisms.begin(), organisms.end(),
              [&](org * a, org * b) -> bool
              {
                  //num_tot++;

                  if(a->rank_fitness == b->rank_fitness)
                  {
                      //num_tie++;
                      return a->crowd_distance > b->crowd_distance;
                  }

                  return a->rank_fitness < b->rank_fitness;
              });

    for (org* i : organisms)
        cout << i->rank_fitness << " " << i->crowd_distance << endl;

    return 0;
}

#endif //MAZESURPRISESEARCH_TESTS_NSGAII_H
